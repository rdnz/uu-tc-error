{-# language TypeApplications #-}

{-# options_ghc
  -Wno-unused-imports
  -Wwarn=missing-signatures
#-}

module ATest where

import ParseLib.Abstract.Core
import ParseLib.Abstract.Derived
import ParseLib.Error
import qualified Data.List as L
import qualified Data.List.NonEmpty as N
import Data.Either (fromLeft)
import Data.Bifunctor (first)
import Data.Maybe (fromMaybe)
import Prelude hiding ((*>), (<*))
import qualified Text.Megaparsec.Error as M

config :: Config
config = errorCountSet (-1) defaultConfig

t0 :: String
t0 =
  errorBundlePretty config "aaa\n\nasdf" $
  ParseErrorBundle $
    [
      (WithLength "sdf" 3, N.fromList [BundledParseError "0\n", BundledParseError "01"]),
      (WithLength "" 0, N.fromList [BundledFail "lol"])
    ]

t1 :: String
t1 =
  fromLeft "" $
  first (errorBundlePretty config input) $
  parseWithConfig config (failp) input
  where
    input :: String
    input = "aaa\n\nasdf"

t2 :: String
t2 =
  fromLeft "" $
  first (errorBundlePretty config input) $
  parseWithConfig config ((traverse symbol "abcde" <|> pure "") <* symbol '.') input
  where
    input :: String
    input = "abce."
--   |
-- 1 | abce.
--   |    ^
-- unexpected 'e'
-- expecting 'd'
-- or
--   |
-- 1 | abce.
--   | ^
-- unexpected 'a'
-- expecting '.'

t3 :: String
t3 =
  fromLeft "" $
  first (errorBundlePretty config input) $
  parseWithConfig config ((symbol 'a' *> symbol 'b') <|> symbol 'a' *> symbol 'c') input
  where
    input :: String
    input = "ad"
--   |
-- 1 | ad
--   |  ^
-- unexpected 'd'
-- expecting 'b' or 'c'

t4 :: String
t4 =
  fromLeft "" $
  first (errorBundlePretty config input) $
  parseWithConfig config ((symbol 'a' *> symbol 'b') <|> symbol 'c') input
  where
    input :: String
    input = "ad"
  --   |
  -- 1 | ad
  --   |  ^
  -- unexpected 'd'
  -- expecting 'b'
  -- or
  --   |
  -- 1 | ad
  --   | ^
  -- unexpected 'a'
  -- expecting 'c'

t5 :: String
t5 =
  fromLeft "" $
  first (errorBundlePretty config input) $
  parseWithConfig config (token "asdf") input
  where
    input :: String
    input = "asde"
-- input:1:1:
--   |
-- 1 | asde
--   | ^^^^
-- unexpected "asde"
-- expecting "asdf"

t6 :: String
t6 =
  fromLeft "" $
  first (errorBundlePretty config input) $
  parseWithConfig config (token "asdf") input
  where
    input :: String
    input = "asd"
-- input:1:1:
--   |
-- 1 | asde
--   | ^^^^
-- unexpected "asd"
-- expecting "asdf"

t7 :: String
t7 =
  fromLeft "" $
  first (errorBundlePretty config input) $
  parseWithConfig config eof input
  where
    input :: String
    input = "a"
-- input:1:1:
--   |
-- 1 | a
--   | ^
-- unexpected 'a'
-- expecting end of input

t8 :: String
t8 =
  fromLeft "" $
  first (errorBundlePrettyImproved config input) $
  parseWithConfig config (empty <|> failp <|> symbol 'a' <|> symbol 'b') input
  where
    input :: String
    input = ""
-- input:1:1:
--   |
-- 1 | <empty line>
--   | ^
-- unexpected end of input
-- expecting 'a' or 'b'
-- input:1:1:
--   |
-- 1 | <empty line>
--   | ^
-- Core.empty
-- Core.failp

data Token = T0 | T1 deriving (Eq, Ord, Show)

t9 :: String
t9 =
  fromLeft "" $
  first (errorBundlePretty config input) $
  parseWithConfig config (empty <|> failp <|> symbol T0 <|> symbol T1 <|> token [T0, T1] *> pure T0) input
  where
    input :: [Token]
    input = []


-- $> putStrLn t2

